package gcuF.gcuonline.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.bean.User;
import gcuF.gcuonline.service.UserSevice;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
	@Resource
	private UserSevice userSevice;
	
	@RequestMapping("/insertUser")
	public String insertUser(User user) {
		return "插入如数据【" + userSevice.insertUser(user) + "】条";
	}
	
	@RequestMapping("/findAll")
	public List<User> findAll(){
		return userSevice.findAll();
	}
}
