package gcuF.gcuonline.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.bean.Food;
import gcuF.gcuonline.bean.FoodBase;
import gcuF.gcuonline.bean.FoodType;
import gcuF.gcuonline.service.FoodTypeService;

@RestController
@RequestMapping("/foodType")
public class FoodTypeController {
	
	@Resource
	private FoodTypeService foodTypeService;
	
	@RequestMapping("/findFoodsByStall")
	public List<FoodType> findFoodsByStall(int stall) {
		List<FoodType> foodTypes = new ArrayList<FoodType>();
		List<String> types = foodTypeService.getFoodsType(stall);
		for (int i = 0; i < types.size(); i++) {
			FoodType foodType = new FoodType();
			foodType.setName(types.get(i));
			foodType.setId(i);
			foodType.setNum(0);
			List<FoodBase> foodBases = new ArrayList<FoodBase>();
			List<Food> foods = foodTypeService.findFoodsByStallAndType(stall,types.get(i));
			for (int j = 0; j < foods.size(); j++) {
				FoodBase foodBase = new FoodBase();
				foodBase.setName(foods.get(j).getName());
				foodBase.setPrice(foods.get(j).getPrice());
				foodBase.setPicture(foods.get(j).getPicture());
				foodBase.setId(foods.get(j).getId());
				foodBase.setNum(0);
				foodBases.add(foodBase);
			}
			foodType.setParts(foodBases);
			foodTypes.add(foodType);
		}
		return foodTypes;
	}
	
}
