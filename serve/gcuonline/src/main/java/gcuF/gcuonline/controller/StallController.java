package gcuF.gcuonline.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.bean.Stall;
import gcuF.gcuonline.service.StallService;

@CrossOrigin
@RestController
@RequestMapping("/stall")
public class StallController {
	@Resource
	private StallService stallService;
	//创建档口
	@RequestMapping("/insertStall")
	public String insertStall(Stall stall) {
		return "插入如数据【" + stallService.insertStall(stall) + "】条";
	}
	//通过食堂地点查看档口
	@RequestMapping("/findStallByPlace")
	public List<Stall> findStallByPlace(int id) {
		return stallService.findStallByPlace(id);
	}
	//查看所有的档口
	@RequestMapping("/findStallAll")
	public List<Stall> findStallAll() {
		return stallService.findStallAll();
	}
	//通过菜名模糊查询
	@RequestMapping("/findStallsByFood")
	public List<Stall> findStallsByFood(String value){
		return stallService.findStallsByFood(value);
	}
	//通过档口名模糊查询
	@RequestMapping("/findStallsByName")
	public List<Stall> findStallsByName(String name){
		return stallService.findStallsByName(name);
	}
	//仅查看档口名和id
	@RequestMapping("/findStallsName")
	public List<Stall> findStallsName(){
		return stallService.findStallsName();
	}
	//删除档口信息
	@RequestMapping("/deleteStallById")
	public int deleteStallById(int id) {
		return stallService.deleteStallById(id);
	}
	//修改档口信息
	@RequestMapping("/upDateStallById")
	public Integer upDateStallById(Stall stall) {
		return stallService.upDateStallById(stall);
	}
	//通过档口id查看档口信息
	@RequestMapping("/selectStallById")
	public Stall selectStallById(int id) {
		return stallService.selectStallById(id);
	}
}
