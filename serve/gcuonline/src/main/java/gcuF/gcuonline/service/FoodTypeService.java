package gcuF.gcuonline.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.bean.Food;
import gcuF.gcuonline.repository.FoodTypeRepository;

@Service
public class FoodTypeService {
	
	@Resource
	private FoodTypeRepository foodTypeRepository;
	
	public List<Food> findFoodsByStallAndType(int stall,String type) {
		return foodTypeRepository.findFoodsByStallAndType(stall,type);
	}
	
	public List<String> getFoodsType(int stall){
		return foodTypeRepository.getFoodsType(stall);
	}

}
