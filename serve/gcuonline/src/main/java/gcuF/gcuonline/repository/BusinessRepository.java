package gcuF.gcuonline.repository;

import org.apache.ibatis.annotations.Insert;

import gcuF.gcuonline.bean.Business;

public interface BusinessRepository {
	
	@Insert("insert into business(boss_id,stall_id)"
			+ "values (#{boss_id},#{stall_id})")
	public int insertBoss(Business boss);
	
}
