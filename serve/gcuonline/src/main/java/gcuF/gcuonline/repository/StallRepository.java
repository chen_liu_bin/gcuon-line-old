package gcuF.gcuonline.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import gcuF.gcuonline.bean.Stall;

public interface StallRepository {
	@Insert("insert into stall(s_name, s_place, s_search, s_boss, s_phone, s_consume, s_introduce, s_time,s_picture)"
			+ "values (#{s_name},#{s_place},#{s_name},#{s_boss},#{s_phone},#{s_consume},#{s_introduce},#{s_time},#{s_picture})")
	public int insertStall(Stall stall);
	
	@Select("select * from stall where s_place=#{id}")
	public List<Stall> findStallByPlace(int id);
	
	@Select("select * from stall")
	public List<Stall> findStallAll();
	
	@Select("select * from stall where s_search like '%${value}%'")
	public List<Stall> findStallsByFood(String value);
	
	@Select("select * from stall where s_name like '%${value}%'")
	public List<Stall> findStallsByName(String value);
	
	@Select("select s_name,s_id from stall")
	public List<Stall> findStallsName();
	
	@Delete("delete from stall where s_id=#{id}")
	public int deleteStallById(int id);
	//修改档口信息
	@Update("update Stall set s_place=#{s_place},s_boss=#{s_boss},s_phone=#{s_phone},"
			+ "s_consume=#{s_consume},s_introduce=#{s_introduce} where s_id = #{s_id}")
	public Integer upDateStallById(Stall stall);

	//查找stall通过id
	@Select("select * from stall where s_id = #{id}")
	public Stall selectStallById(int id);

}