package gcuF.gcuonline.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import gcuF.gcuonline.bean.User;

public interface UserRepository {
	@Insert("insert into user(username,user_id)"
			+ "values (#{username},#{user_id})")
	public int insertUser(User user);
	
	@Select("select * from user where user_id = #{user_id}")
	public List<User> isHave(String user_id);
	
	@Select("select * from user")
	public List<User> findAll();
}
