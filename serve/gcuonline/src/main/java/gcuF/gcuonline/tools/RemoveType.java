package gcuF.gcuonline.tools;

import org.springframework.stereotype.Component;

@Component
public class RemoveType {
//	去掉type中系列二字
	public String removeString(String string) {
		char [] toArray = string.toCharArray();
		int index = toArray.length;
		System.out.println(index);
		if (toArray[index-2] == '系' && toArray[index-1] == '列') {
			string = string.replace("系列", "");
		}
		return string;
	}
}
