package gcuF.gcuonline.tools;

import org.springframework.stereotype.Component;

@Component
public class UpdateSearch {
	public String newS_Search(String s_search,String string) {
		//删掉该关键字
		s_search = s_search.replace(string, "");
		//防止出现两个顿号
		s_search = s_search.replace("、、", "、");
		return s_search;
	}
}
