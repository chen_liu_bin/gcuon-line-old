import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import Login from '../components/Login.vue'
import Territory from '../components/territory/Territory.vue'
import Food from '../components/food/Food.vue'
import Community from '../components/bzdjshd/Community.vue'
import Homepage from '../components/bzdjshd/Homepage.vue'
import User from '../components/bzdjshd/User.vue'

Vue.use(VueRouter)

const routes = [
  {path:'/', redirect:'/login'},
  {path:'/login',component:Login},
  { path: '/home',
  component: Home,
  redirect: '/welcome',
  children: [
    { path: '/welcome', component: Welcome },
    { path: '/territory', component:Territory},
    { path: '/food', component:Food},
    { path: '/community', component:Community},
    { path: '/user', component:User},
    { path: '/homepage', component:Homepage}
  ] },
  //redirect:'/welcome',
  //添加路由的另一种方式
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  /* {
    path:'/test',
    component:Test
  } */
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
