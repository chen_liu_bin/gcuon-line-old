import Vue from 'vue'
import { 
    Button, 
    Form,
    FormItem,
    Input,
    Container, 
    Header, 
    Aside, 
    Main,
    Menu,
    Submenu,
    MenuItemGroup,
    MenuItem,
    Message,
    Breadcrumb,
    BreadcrumbItem,
    Card,
    Table,
    TableColumn,
    Row,
    Col,
    Switch,
    Pagination,
    Dialog,
    MessageBox,
    Select,
    Option
} from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Menu)
Vue.use(Submenu)
Vue.use(MenuItemGroup)
Vue.use(MenuItem)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Card)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Row)
Vue.use(Col)
Vue.use(Switch)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Select)
Vue.use(Option)
