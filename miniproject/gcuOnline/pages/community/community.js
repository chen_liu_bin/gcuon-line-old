// pages/community/community.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    src:'https://www.bilibili.com/video/BV1C5411j7K3'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  on: function () {
    var that = this
    wx.chooseVideo({
      sourceType:['album','camera'],
      maxDuration:60,
      camera: ['front','back'],
      success:function(res){
        that.setData({
          src:res.tempFilePath
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})