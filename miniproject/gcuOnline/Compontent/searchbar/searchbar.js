// Compontent/searchbar/searchbar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isnavigator:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    str:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    OnClearTouchClick: function (event) {
      console.log(event)
      this.setData({
        str: ""
      })
      var value = event.detail.value;
      var detail = { "value": value }
      var options = {};
      this.triggerEvent("cancleClick", detail, options);
    },
    Click:function(event){
      var value = event.detail.value;
      var detail = {"value":value}
      var options = {};
      this.triggerEvent("searchInput",detail,options);
    }
  }
})
